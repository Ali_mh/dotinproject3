function validateForm() {
    var field = document.forms["form"]["first_name"].value;
    var field2 = document.forms["form"]["birthday_day"].value;
    var field3 = document.forms["form"]["birthday_month"].value;
    var field4 = document.forms["form"]["birthday_year"].value;
    var field5 = document.forms["form"]["national_code"].value;

    if (field == null || field.trim() == "") {
        alert(messages.legal_company_name_empty);
        document.forms["form"]["first_name"].focus();
        return false;
    }
    if (field2 == null || field2.trim() == "") {
        alert(messages.legal_registration_day_empty);
        document.forms["form"]["birthday_day"].focus();
        return false;
    }
    if (field2 > 31 || field2 < 1) {
        alert(messages.legal_registration_day_invalid);
        document.forms["form"]["birthday_day"].focus();
        return false;
    }
    if (field3 == null || field3.trim() == "") {
        alert(messages.legal_registration_month_empty);
        document.forms["form"]["birthday_month"].focus();
        return false;
    }
    if (field3 > 12 || field3 < 1) {
        alert(messages.legal_registration_month_invalid);
        document.forms["form"]["birthday_month"].focus();
        return false;
    }

    if (field4 == null || field4.trim() == "") {
        alert(messages.legal_registration_year_empty);
        document.forms["form"]["birthday_year"].focus();
        return false;
    }
    if (field4 > 1500 || field4 < 1000) {
        alert(messages.legal_registration_year_invalid);
        document.forms["form"]["birthday_year"].focus();
        return false;
    }
    if (field5 == null || field5.trim() == "") {
        alert(messages.legal_economic_code_empty);
        document.forms["form"]["national_code"].focus();
        return false;
    }
    if (!(field5.trim().length == 12 || field5.trim().length == 16)) {
        alert(messages.legal_economic_code_invalid_length);
        document.forms["form"]["national_code"].focus();
        return false;
    }
    if (isNaN(field5)) {
        alert(messages.legal_economic_code_invalid);
        document.forms["form"]["national_code"].focus();
        return false;
    }
}

function isNumber() {
    var charCode = event.charCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function getFocus1() {
    var field = document.getElementById("birthday_day").value;
    if (field.length >= 2)
        document.getElementById("birthday_month").focus();
}

function getFocus2() {
    var field2 = document.getElementById("birthday_month").value;
    if (field2.length >= 2)
        document.getElementById("birthday_year").focus();
}


function validateForm2() {

    var field = document.forms["form"]["national_code"].value;
    var field2 = document.forms["form"]["customer_id"].value;

    if ((field != "") && (!(field.length == 12 || field.length == 16))) {
        alert(messages.legal_economic_code_invalid_length);
        document.forms["form"]["national_code"].focus();
        return false;
    }
    if (field2 < 0 || field2 > 2147483647) {
        alert(messages.id_invalid_length);
        document.forms["form"]["customer_id"].focus();
        return false;
    }
    if (isNaN(field)) {
        alert(messages.legal_economic_code_invalid);
        document.forms["form"]["national_code"].focus();
        return false;
    }
    if (isNaN(field2)) {
        alert(messages.id_invalid);
        document.forms["form"]["customer_id"].focus();
        return false;
    }


}