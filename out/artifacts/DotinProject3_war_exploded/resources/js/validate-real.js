function validateForm() {
    var field = document.forms["form"]["first_name"].value;
    var field2 = document.forms["form"]["last_name"].value;
    var field3 = document.forms["form"]["father_name"].value;
    var field4 = document.forms["form"]["birthday_day"].value;
    var field5 = document.forms["form"]["birthday_month"].value;
    var field6 = document.forms["form"]["birthday_year"].value;
    var field7 = document.forms["form"]["national_code"].value;


    if (field == null || field.trim() == "") {
        alert(messages.real_first_name_empty);
        document.forms["form"]["first_name"].focus();
        return false;
    }
    if (field2 == null || field2.trim() == "") {
        alert(messages.real_last_name_empty);
        document.forms["form"]["last_name"].focus();
        return false;
    }
    if (field3 == null || field3.trim() == "") {
        alert(messages.real_father_name_empty);
        document.forms["form"]["father_name"].focus();
        return false;
    }
    if (field4 == null || field4.trim() == "") {
        alert(messages.real_birth_day_empty);
        document.forms["form"]["birthday_day"].focus();
        return false;
    }
    if (field4 > 31 || field4 < 1) {
        alert(messages.real_birth_day_invalid);
        document.forms["form"]["birthday_day"].focus();
        return false;
    }
    if (field5 == null || field5.trim() == "") {
        alert(messages.real_birth_month_empty);
        document.forms["form"]["birthday_month"].focus();
        return false;
    }
    if (field5 > 12 || field5 < 1) {
        alert(messages.real_birth_month_invalid);
        document.forms["form"]["birthday_month"].focus();
        return false;
    }

    if (field6 == null || field6.trim() == "") {
        alert(messages.real_birth_year_empty);
        document.forms["form"]["birthday_year"].focus();
        return false;
    }
    if (field6 > 1500 || field6 < 1000) {
        alert(messages.real_birth_year_invalid);
        document.forms["form"]["birthday_year"].focus();
        return false;
    }
    if (field7 == null || field7.trim() == "") {
        alert(messages.real_national_code_empty);
        document.forms["form"]["national_code"].focus();
        return false;
    }
    if (field7.trim().length < 10 || field7.trim().length > 10) {
        alert(messages.real_national_code_invalid_length);
        document.forms["form"]["national_code"].focus();
        return false;
    }
    if (isNaN(field7)) {
        alert(messages.real_national_code_invalid);
        document.forms["form"]["national_code"].focus();
        return false;
    }

}

function isNumber() {
    var charCode = event.charCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function getFocus1() {
    var field = document.getElementById("birthday_d").value;
    if (field.length >= 2)
        document.getElementById("birthday_m").focus();
}

function getFocus2() {
    var field2 = document.getElementById("birthday_m").value;
    if (field2.length >= 2)
        document.getElementById("birthday_y").focus();
}


function validateForm2() {

    var field = document.forms["form"]["national_code"].value;
    var field2 = document.forms["form"]["customer_id"].value;

    if ((field != "") && (field.length < 10 || field.length > 10)) {
        alert(messages.real_national_code_invalid_length);
        document.forms["form"]["national_code"].focus();
        return false;
    }
    if (field2 < 0 || field2 > 2147483647) {
        alert(messages.id_invalid_length);
        document.forms["form"]["customer_id"].focus();
        return false;
    }
    if (isNaN(field)) {
        alert(messages.real_national_code_invalid);
        document.forms["form"]["national_code"].focus();
        return false;
    }
    if (isNaN(field2)) {
        alert(messages.id_invalid);
        document.forms["form"]["customer_id"].focus();
        return false;
    }


}



