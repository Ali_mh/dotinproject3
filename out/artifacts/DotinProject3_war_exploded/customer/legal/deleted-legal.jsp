<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% ResourceBundle resourceBundle = ResourceBundle.getBundle(
        "Customer");%>
<!DOCTYPE html>
<html>
<head>
    <title><%=resourceBundle.getString("delete_legal_result_title")%>
    </title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/indexCss.css">

</head>
<body>
<jsp:include page="/navbar.jsp"/>

<% if (request.getParameter("del") == null)
    return;
    if (request.getParameter("del").equals("1")) {
%>
<div class="form">
    <p><%=resourceBundle.getString("delete_legal_success")%>
    </p>
    <p><a href="${pageContext.request.contextPath}/customer/legal/search-legal.jsp" style="font-size: 16px">
        <%=resourceBundle.getString("back_to_legal_search")%>
    </a></p>

</div>
<%} else {%>
<div class="form">
    <p><%=resourceBundle.getString("delete_legal_fail")%>
    </p>
    <p><a href="${pageContext.request.contextPath}/customer/legal/search-legal.jsp" style="font-size: 16px">
        <%=resourceBundle.getString("back_to_legal_search")%>
    </a></p>

</div>
<%}%>


</body>
</html>
