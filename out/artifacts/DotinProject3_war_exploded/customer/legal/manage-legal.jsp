<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% ResourceBundle resourceBundle = ResourceBundle.getBundle(
        "Customer");%>
<!DOCTYPE html>
<html>
<head>
    <title><%=resourceBundle.getString("manage_legal_title")%></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/indexCss.css">

</head>

<body>
<jsp:include page="/navbar.jsp"/>
<div class="form">
    <p><%=resourceBundle.getString("manage_legal")%></p>
    <br><br>
    <p>
        <a href="add-legal.jsp"><button class="button" type="button"><%=resourceBundle.getString("manage_legal_register_btn")%></button></a>
        <a href="search-legal.jsp"><button class="button" type="button"><%=resourceBundle.getString("manage_legal_search_btn")%></button></a></p></div>
</body>
</html>