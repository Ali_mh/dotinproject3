<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% ResourceBundle resourceBundle = ResourceBundle.getBundle(
        "Customer");%>
<!DOCTYPE html>
<html>
<head>
    <title><%=resourceBundle.getString("edited_legal_title")%>
    </title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/indexCss.css">

</head>
<body>
<jsp:include page="/navbar.jsp"/>
<%
    if (request.getParameter("edited") == null)
        return;
    if (request.getParameter("edited").equals("1")) {
%>
<div class="form">
    <p><%=resourceBundle.getString("edited_legal_success")%></p>

    <p><a href="search-legal.jsp" style="font-size: 16px"><%=resourceBundle.getString("back_to_legal_search")%>
    </a></p>

</div>
<%
} else if (request.getParameter("edited").equals("0")) {
%>
<div class="form">
    <p><%=resourceBundle.getString("edited_legal_fail_economic_code")%></p>

    <p><a href="search-legal.jsp" style="font-size: 16px"><%=resourceBundle.getString("back_to_legal_search")%>
    </a></p>
</div>

<%}%>

</body>
</html>
