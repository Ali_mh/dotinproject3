<%@ page import="java.util.List" %>
<%@ page import="com.dotin.customermanager.service.Converter" %>
<%@ page import="com.dotin.customermanager.model.Customer" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% ResourceBundle resourceBundle = ResourceBundle.getBundle(
        "Customer");%>
<!DOCTYPE html>
<html>
<head>
    <title><%=resourceBundle.getString("search_real_result_title")%>
    </title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/tableCss.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/indexCss.css">
</head>
<body>
<jsp:include page="/navbar.jsp"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/customer-properties.js"></script>

<%
    if (request.getAttribute("found") == null) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        return;
    }
    if (!(request.getAttribute("found").equals(true))) {
%>

<div class="form">
    <p><%=resourceBundle.getString("search_real_not_found")%>
    </p>
    <p><a href="${pageContext.request.contextPath}/customer/real/search-real.jsp"
          style="font-size: 16px"><%=resourceBundle.getString("back_to_real_search")%>
    </a></p>

</div>

<%
} else {
    if (request.getAttribute("customerList") == null) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        return;
    }

%>

<div style="text-align:center;direction: rtl;font-family:'B Nazanin',cursive;font-size: 40px">
    <b><%=resourceBundle.getString("search_real_results")%>
    </b>
</div>
<br>
<div>
    <table>
        <tr>
            <th>
                <%=resourceBundle.getString("row")%>
            </th>
            <th>
                <%=resourceBundle.getString("customer_id")%>
            </th>
            <th>
                <%=resourceBundle.getString("first_name")%>
            </th>
            <th>
                <%=resourceBundle.getString("last_name")%>
            </th>
            <th>
                <%=resourceBundle.getString("father_name")%>
            </th>
            <th>
                <%=resourceBundle.getString("national_code")%>
            </th>
            <th>
                <%=resourceBundle.getString("birthday")%>
            </th>
            <th>
                <%=resourceBundle.getString("delete_link_lbl")%>
            </th>
            <th>
                <%=resourceBundle.getString("edit_link_lbl")%>
            </th>

        </tr>
        <% int i = 1;
            List<Customer> customerList = (List<Customer>) request.getAttribute("customerList");
            for (Customer customer : customerList) {
        %>
        <tr>
            <td>
                <%= i++%>
            </td>
            <td>
                <%= customer.getId()%>
            </td>
            <td>
                <%= customer.getFirstName()%>
            </td>
            <td>
                <%= customer.getLastName()%>
            </td>
            <td>
                <%= customer.getFatherName()%>
            </td>
            <td>
                <%= customer.getNationalCode()%>
            </td>
            <td>
                <%=Converter.jalaliDate(customer.getBirthday())%>
            </td>
            <td>
                <a class="confirmation"
                   href="${pageContext.request.contextPath}/customer/edit-customer?action=delete&type=<%=customer.getCustomerType()%>&customer_id=<%=customer.getId()%>"><%=resourceBundle.getString("delete_link_lbl")%>
                </a>
            </td>
            <td>
                <a href="${pageContext.request.contextPath}/customer/edit-customer?action=edit&type=<%=customer.getCustomerType()%>&customer_id=<%=customer.getId()%>"><%=resourceBundle.getString("edit_link_lbl")%>
                </a>
            </td>

        </tr>
        <%}%>
    </table>
        <%}%>

    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/customer-properties.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/del-confirmation.js"></script>
</body>
</html>
