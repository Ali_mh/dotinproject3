
<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% ResourceBundle resourceBundle = ResourceBundle.getBundle(
        "Customer");%>
<div class="navbar" >
    <a href="${pageContext.request.contextPath}/index.jsp"><%=resourceBundle.getString("navbar_homepage")%></a>
    <div class="dropdown">
        <button class="dropbtn"><%=resourceBundle.getString("index_real_customer_btn")%>
        </button>
        <div class="dropdown-content">
            <a href="${pageContext.request.contextPath}/customer/real/search-real.jsp"><%=resourceBundle.getString("manage_real_search_btn")%></a>
            <a href="${pageContext.request.contextPath}/customer/real/add-real.jsp"><%=resourceBundle.getString("manage_real_register_btn")%></a>
        </div>
    </div>
    <div class="dropdown">
        <button class="dropbtn"><%=resourceBundle.getString("index_legal_customer_btn")%>
        </button>
        <div class="dropdown-content">
            <a href="${pageContext.request.contextPath}/customer/legal/search-legal.jsp"><%=resourceBundle.getString("manage_legal_search_btn")%></a>
            <a href="${pageContext.request.contextPath}/customer/legal/add-legal.jsp"><%=resourceBundle.getString("manage_legal_register_btn")%></a>
        </div>
    </div>

</div>
<div id="header">
</div>
