var messages= {
    id_invalid:"شماره مشتری باید عدد صحیح باشد",
    id_invalid_length:"شماره مشتری نباید بیشتر از 2147483647 باشد",
    delete_confirmation:"آیا مشتری حذف شود؟",

    real_first_name_empty:"لطفاً نام را وارد نمایید" ,
    real_last_name_empty:"لطفاً نام خانوادگی را وارد نمایید",
    real_father_name_empty:"لطفاً نام پدر را وارد نمایید",
    real_birth_day_empty:"لطفاً روز تولد را وارد نمایید",
    real_birth_day_invalid:"لطفاً روز تولد را به درستی وارد نمایید",
    real_birth_month_empty:"لطفاً ماه تولد را وارد نمایید",
    real_birth_month_invalid:"لطفاً ماه تولد را به درستی وارد نمایید",
    real_birth_year_empty:"لطفاً سال تولد را وارد نمایید",
    real_birth_year_invalid:"لطفاً سال تولد را به صورت هجری شمسی وارد نمایید",
    real_national_code_empty:"لطفاً کد ملی را وارد نمایید",
    real_national_code_invalid:" کد ملی باید عدد صحیح باشد",
    real_national_code_invalid_length:"کد ملی باید 10 رقم باشد",

    legal_company_name_empty:"لطفاً نام شرکت را وارد نمایید",
    legal_registration_day_empty:"لطفاً روز ثبت را وارد نمایید",
    legal_registration_day_invalid:"لطفاً روز ثبت را به درستی وارد نمایید",
    legal_registration_month_empty:"لطفاً ماه ثبت را وارد نمایید",
    legal_registration_month_invalid:"لطفاً ماه ثبت را به درستی وارد نمایید",
    legal_registration_year_empty:"لطفاً سال ثبت را وارد نمایید",
    legal_registration_year_invalid:"لطفاً سال ثبت را به صورت هجری شمسی وارد نمایید",
    legal_economic_code_empty:"لطفاً کد اقتصادی را وارد نمایید",
    legal_economic_code_invalid_length:"کد اقتصادی باید 12 یا 16 رفم باشد",
    legal_economic_code_invalid:"کد اقتصادی باید عدد صحیح باشد",


}