<%@ page import="com.dotin.customermanager.enums.CustomerType" %>
<%@ page import="com.dotin.customermanager.service.Converter" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% ResourceBundle resourceBundle = ResourceBundle.getBundle(
        "Customer");%>
<!DOCTYPE html>
<html>
<head>
    <title><%=resourceBundle.getString("edit_real_title")%></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/indexCss.css">
</head>
<body>
<jsp:include page="/navbar.jsp"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/customer-properties.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/validate-real.js"></script>
<%
    if (request.getAttribute("customer") == null) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        return;
    }
%>
<jsp:useBean id="customer" class="com.dotin.customermanager.model.Customer" scope="request"/>
<div>
    <form class="form" name="form" method="post" action="${pageContext.request.contextPath}/customer/edit-customer"
          onsubmit="return validateForm();">
        <p><b><%=resourceBundle.getString("edit_real")%></b></p>
        <table>
            <tr>
                <td>
                    <label for="first_name"><%=resourceBundle.getString("first_name")%>:</label>
                </td>
                <td>
                    <input id="first_name" maxlength="30" value="${customer.firstName}" type="text" name="first_name"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="last_name"><%=resourceBundle.getString("last_name")%>:</label>
                </td>
                <td>
                    <input id="last_name" maxlength="45" value="${customer.lastName}" type="text" name="last_name"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="father_name"><%=resourceBundle.getString("father_name")%>:</label>
                </td>
                <td>
                    <input id="father_name" maxlength="30" value="${customer.fatherName}" type="text"
                           name="father_name"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="birthday_d"><%=resourceBundle.getString("birthday")%>:</label>
                </td>
                <td><%int[] dateItem = Converter.jalaliDateItems(customer.getBirthday()); %>
                    <input id="birthday_d" value="<%=dateItem[2]%>" name="birthday_day" maxlength="2" size="1"
                           placeholder="<%=resourceBundle.getString("day")%>" onkeypress="return isNumber()" onkeyup="getFocus1()"/>
                    <input id="birthday_m" value="<%=dateItem[1]%>" name="birthday_month" maxlength="2" size="1"
                           placeholder="<%=resourceBundle.getString("month")%>" onkeypress="return isNumber()" onkeyup="getFocus2()"/>
                    <input id="birthday_y" value="<%=dateItem[0]%>" name="birthday_year" maxlength="4" size="3"
                           placeholder="<%=resourceBundle.getString("year")%>" onkeypress="return isNumber()"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="national_code"><%=resourceBundle.getString("national_code")%>:</label>
                </td>
                <td>
                    <input id="national_code" maxlength="10" value="${customer.nationalCode}" type="text"
                           name="national_code" onkeypress="return isNumber()"/>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <input type="hidden" name="customer_type" value="${CustomerType.REAL}"/>
                    <input type="hidden" name="customer_id" value="${customer.id}" />
                    <input class="button2" type="submit" value="<%=resourceBundle.getString("edit_real_btn")%>"/>
                </td>
            </tr>
        </table>

    </form>

</div>

</body>
</html>