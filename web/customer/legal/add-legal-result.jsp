<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% ResourceBundle resourceBundle = ResourceBundle.getBundle(
        "Customer");%>
<!DOCTYPE html>
<html>
<head>
    <title><%=resourceBundle.getString("register_legal_result_title")%>
    </title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/indexCss.css">
</head>
<body>
<jsp:include page="/navbar.jsp"/>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/customer-properties.js"></script>

<jsp:useBean id="customer" class="com.dotin.customermanager.model.Customer" scope="request"/>
<% if (request.getAttribute("found") == null) {
    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
    return;
}
    if (request.getAttribute("found").equals(true)) { %>
<div class="form" style="text-align:center;direction: rtl;font-family:'B Nazanin',cursive;font-size: 20px">
    <%=resourceBundle.getString("register_failed_economic_code")%>
    <br><br>
    <a href="${pageContext.request.contextPath}/customer/legal/add-legal.jsp" style="font-size: 16px">
        <%=resourceBundle.getString("back_to_legal_register")%>
    </a>
</div>
<%} else { %>

<div class="form" style="text-align:center;direction: rtl;font-family:'B Nazanin',cursive;font-size: 20px">
    <%=resourceBundle.getString("register_legal_success")%>
    <br>
    <%=resourceBundle.getString("register_legal_result_id")%> ${customer.id}
    <br><br>
    <a href="${pageContext.request.contextPath}/customer/legal/add-legal.jsp" style="font-size: 16px">
        <%=resourceBundle.getString("back_to_legal_register")%>
    </a>
</div>
<%}%>
</body>
</html>
