
<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% ResourceBundle resourceBundle = ResourceBundle.getBundle(
        "Customer");%>
<!DOCTYPE html>
<html>
<head>
    <title><%=resourceBundle.getString("index_title")%></title>
    <link rel="stylesheet" type="text/css" href="resources/css/indexCss.css">
</head>

<body>
<jsp:include page="navbar.jsp"/>

<div class="form">
<p><%=resourceBundle.getString("index_subject")%></p>
<br><br>
<p>
    <a href="customer/real/manage-real.jsp"><button class="button" type="button"><%=resourceBundle.getString("index_real_customer_btn")%></button></a>
    <a href="customer/legal/manage-legal.jsp"><button class="button" type="button"><%=resourceBundle.getString("index_legal_customer_btn")%></button></a></p>
</div>
</body>
</html>
