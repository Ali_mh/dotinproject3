package com.dotin.customermanager.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.logging.Logger;


public class DBConnector {
    private static final Logger logger = Logger.getLogger("global");
    private final static String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
    private final static String DB_URL = "jdbc:mysql://localhost:3306/user";
    private final static String DB_USER = "Admin";
    private final static String DB_PASS = "1234";
    private Connection connection;

    public Connection getConnection() {
        if (connection == null) {
            try {
                Class.forName(DB_DRIVER);
                this.connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
            } catch (Exception e) {
                logger.severe("Connection creation failed!" + e.getMessage());
            }

        } else {
            logger.warning("Connection already exists!");

        }
        return connection;
    }


    public void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
            } catch (Exception e) {
                logger.severe("Connection closing failed!" + e.getMessage());
            }
        }
    }

}