package com.dotin.customermanager.servlet;

import com.dotin.customermanager.dao.CustomerDao;
import com.dotin.customermanager.enums.CustomerType;
import com.dotin.customermanager.model.Customer;
import com.dotin.customermanager.service.Converter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

@WebServlet(name = "EditCustomerServlet", urlPatterns = "/customer/edit-customer")
public class EditCustomerServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        String firstName = request.getParameter("first_name");
        String lastName = request.getParameter("last_name");
        String fatherName = request.getParameter("father_name");
        String birthDay = request.getParameter("birthday_day");
        String birthMonth = request.getParameter("birthday_month");
        String birthYear = request.getParameter("birthday_year");
        String nationalCode = request.getParameter("national_code");
        String id = request.getParameter("customer_id");
        String type = request.getParameter("customer_type");
        CustomerType customerType;

        if (type == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        try {
            customerType = CustomerType.valueOf(type);
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }


        if (customerType.equals(CustomerType.REAL)) {

            if (firstName == null || firstName.trim().equals("") || lastName == null || lastName.trim().equals("")
                    || fatherName == null || fatherName.trim().equals("") || birthDay == null || birthDay.trim().equals("")
                    || birthMonth == null || birthMonth.trim().equals("") || birthYear == null || birthYear.trim().equals("")
                    || nationalCode == null || nationalCode.trim().equals("") || id == null || id.trim().equals("")) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);

            } else if (firstName.length() > 30 || lastName.length() > 45 || fatherName.length() > 30 || nationalCode.length() != 10
                    || birthDay.length() > 2 || birthMonth.length() > 2 || birthYear.length() > 4) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);

            } else {
                Customer customer = new Customer();
                try {
                    int birthdayD = Integer.parseInt(birthDay);
                    int birthdayM = Integer.parseInt(birthMonth);
                    int birthdayY = Integer.parseInt(birthYear);
                    if (birthdayD < 1 || birthdayD > 31 || birthdayM < 1 || birthdayM > 12 || birthdayY < 1000 || birthdayY > 1500)
                        throw new Exception("Invalid Date.");
                    int[] birthdayConvert = Converter.jalali_to_gregorian(birthdayY, birthdayM, birthdayD);
                    java.sql.Date birthday = Date.valueOf(birthdayConvert[0] + "-" + birthdayConvert[1] + "-" + birthdayConvert[2]);
                    customer.setBirthday(birthday);
                    customer.setId(Integer.parseInt(id));
                    customer.setFirstName(firstName);
                    customer.setLastName(lastName);
                    customer.setFatherName(fatherName);
                    Long.parseLong(nationalCode);
                    customer.setNationalCode(nationalCode);

                } catch (Exception e) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                CustomerDao customerDao = new CustomerDao();
                int affected = customerDao.update(customer);
                if (affected != 0) {
                    response.sendRedirect(getServletContext().getContextPath() + "/customer/real/edited-real.jsp?edited=1");
                } else {
                    response.sendRedirect(getServletContext().getContextPath() + "/customer/real/edited-real.jsp?edited=0");
                }
            }


        } else if (customerType.equals(CustomerType.LEGAL)) {

            if (firstName == null || firstName.trim().equals("") || birthDay == null || birthDay.trim().equals("")
                    || birthMonth == null || birthMonth.trim().equals("") || birthYear == null ||
                    birthYear.trim().equals("") || id == null || id.trim().equals("") || nationalCode == null ||
                    nationalCode.trim().equals("")) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);

            } else if (birthDay.length() > 2 || birthMonth.length() > 2 || birthYear.length() > 4 ||
                    (!(nationalCode.length() == 12 || nationalCode.length() == 16) || firstName.length() > 45) || id.length() > 10) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);

            } else {
                Customer customer = new Customer();
                try {
                    int registrationD = Integer.parseInt(birthDay);
                    int registrationM = Integer.parseInt(birthMonth);
                    int registrationY = Integer.parseInt(birthYear);
                    if (registrationD < 1 || registrationD > 31 || registrationM < 1 || registrationM > 12 || registrationY < 1000 || registrationY > 1500)
                        throw new Exception("Invalid Date.");
                    int[] regDateConvert = Converter.jalali_to_gregorian(registrationY, registrationM, registrationD);
                    Date registrationDate = Date.valueOf(regDateConvert[0] + "-" + regDateConvert[1] + "-" + regDateConvert[2]);
                    customer.setBirthday(registrationDate);
                    Long.valueOf(nationalCode);
                    customer.setNationalCode(nationalCode);
                    customer.setFirstName(firstName);
                    customer.setId(Integer.valueOf(id));

                } catch (Exception e) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                CustomerDao customerDao = new CustomerDao();
                int affected = customerDao.update(customer);
                if (affected != 0) {
                    response.sendRedirect(getServletContext().getContextPath() + "/customer/legal/edited-legal.jsp?edited=1");
                } else {
                    response.sendRedirect(getServletContext().getContextPath() + "/customer/legal/edited-legal.jsp?edited=0");
                }
            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = request.getParameter("action");
        String id = request.getParameter("customer_id");
        String type = request.getParameter("type");

        if (!(action == null || action.trim().equals(""))) {
            if (id == null || id.trim().equals("") || type == null || type.trim().equals("")) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            if (action.equals("delete")) {
                CustomerDao customerDao = new CustomerDao();
                try {
                    int affected = customerDao.delete(Integer.parseInt(id));
                    Integer typeIndex = Integer.parseInt(type);
                    if (affected != 0) {
                        if (typeIndex.equals(CustomerType.REAL.getTypeIndex()))
                            response.sendRedirect(getServletContext().getContextPath() + "/customer/real/deleted-real.jsp?del=1");
                        else
                            response.sendRedirect(getServletContext().getContextPath() + "/customer/legal/deleted-legal.jsp?del=1");

                    } else {
                        if (typeIndex.equals(CustomerType.REAL.getTypeIndex()))
                            response.sendRedirect(getServletContext().getContextPath() + "/customer/real/deleted-real.jsp?del=0");
                        else
                            response.sendRedirect(getServletContext().getContextPath() + "/customer/legal/deleted-legal.jsp?del=0");

                    }
                } catch (Exception e) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                }
            } else if (action.equals("edit")) {
                Customer customer = new Customer();
                CustomerDao customerDao = new CustomerDao();
                try {
                    customer.setId(Integer.parseInt(id));
                    int typeIndex = Integer.parseInt(type);
                    CustomerType customerType = CustomerType.getCustomerType(typeIndex);
                    if (customerType == null)
                        throw new Exception("CustomerType does not exist!");
                    customer.setCustomerType(customerType.getTypeIndex());

                    List<Customer> customerList = customerDao.select(customer);
                    customer = customerList.get(0);
                    request.setAttribute("customer", customer);
                    if (customer.getCustomerType().equals(CustomerType.REAL.getTypeIndex())) {
                        request.getRequestDispatcher("/customer/real/edit-real.jsp").forward(request, response);
                    } else if (customer.getCustomerType().equals(CustomerType.LEGAL.getTypeIndex())) {
                        request.getRequestDispatcher("/customer/legal/edit-legal.jsp").forward(request, response);
                    }
                } catch (Exception e) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                }

            }


        }


    }
}
