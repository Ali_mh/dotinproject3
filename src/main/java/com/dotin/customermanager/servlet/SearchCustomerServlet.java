package com.dotin.customermanager.servlet;

import com.dotin.customermanager.dao.CustomerDao;
import com.dotin.customermanager.enums.CustomerType;
import com.dotin.customermanager.model.Customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SearchCustomerServlet", urlPatterns = "/customer/search-customer")
public class SearchCustomerServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String firstName = request.getParameter("first_name");
        String lastName = request.getParameter("last_name");
        String nationalCode = request.getParameter("national_code");
        String id = request.getParameter("customer_id");
        String type = request.getParameter("customer_type");
        CustomerType customerType;

        if (type == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        try {
            customerType = CustomerType.valueOf(type);
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }


        if (customerType.equals(CustomerType.REAL)) {
            if (firstName == null || lastName == null || nationalCode == null || id == null
                    || firstName.length() > 30 || lastName.length() > 45 || id.length() > 10
                    || (nationalCode.length() != 0 && nationalCode.length() != 10)) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } else {
                Customer customer = new Customer();
                try {
                    customer.setCustomerType(customerType.getTypeIndex());
                    if (!(id.trim().equals("")))
                        customer.setId(Integer.parseInt(id));
                    if (!(firstName.trim().equals("")))
                        customer.setFirstName(firstName);
                    if (!(lastName.trim().equals("")))
                        customer.setLastName(lastName);
                    if (!(nationalCode.trim().equals("")))
                        customer.setNationalCode(nationalCode);
                } catch (Exception e) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                CustomerDao customerDao = new CustomerDao();
                List<Customer> customerList = customerDao.select(customer);

                if (customerList.isEmpty()) {
                    request.setAttribute("found", false);
                    request.getRequestDispatcher("/customer/real/search-real-result.jsp").forward(request, response);
                    return;
                }
                request.setAttribute("found", true);
                request.setAttribute("customerList", customerList);
                request.getRequestDispatcher("/customer/real/search-real-result.jsp").forward(request, response);

            }
        } else if (customerType.equals(CustomerType.LEGAL)) {

            if (firstName == null || nationalCode == null || id == null || firstName.length() > 45 ||
                    nationalCode.length() > 16) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } else {
                Customer customer = new Customer();
                try {
                    if (!(id.trim().equals("")))
                        customer.setId(Integer.parseInt(id));
                    if (!(firstName.trim().equals("")))
                        customer.setFirstName(firstName);
                    if (!(nationalCode.trim().equals("")))
                        customer.setNationalCode(nationalCode);
                    customer.setCustomerType(customerType.getTypeIndex());
                } catch (Exception e) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                CustomerDao customerDao = new CustomerDao();
                List<Customer> customerList = customerDao.select(customer);
                if (customerList.isEmpty()) {

                    request.setAttribute("found", false);
                    request.getRequestDispatcher("/customer/legal/search-legal-result.jsp").forward(request, response);
                    return;
                }
                request.setAttribute("found", true);
                request.setAttribute("customerList", customerList);
                request.getRequestDispatcher("/customer/legal/search-legal-result.jsp").forward(request, response);


            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
