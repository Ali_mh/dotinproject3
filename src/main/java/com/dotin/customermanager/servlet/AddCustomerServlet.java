package com.dotin.customermanager.servlet;

import com.dotin.customermanager.dao.CustomerDao;
import com.dotin.customermanager.enums.CustomerType;
import com.dotin.customermanager.model.Customer;
import com.dotin.customermanager.service.Converter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.List;


@WebServlet(name = "AddCustomerServlet", urlPatterns = "/customer/add-customer")
public class AddCustomerServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        String firstName = request.getParameter("first_name");
        String lastName = request.getParameter("last_name");
        String fatherName = request.getParameter("father_name");
        String birthDay = request.getParameter("birthday_day");
        String birthMonth = request.getParameter("birthday_month");
        String birthYear = request.getParameter("birthday_year");
        String nationalCode = request.getParameter("national_code");
        String type = request.getParameter("customer_type");
        CustomerType customerType;
        if (type == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        try {
            customerType = CustomerType.valueOf(type);
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        if (customerType.equals(CustomerType.REAL)) {

            if (firstName == null || firstName.trim().equals("") || lastName == null || lastName.trim().equals("")
                    || fatherName == null || fatherName.trim().equals("") || birthDay == null || birthDay.trim().equals("")
                    || birthMonth == null || birthMonth.trim().equals("") || birthYear == null || birthYear.trim().equals("")
                    || nationalCode == null || nationalCode.trim().equals("")) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);

            } else if (firstName.length() > 30 || lastName.length() > 45 || fatherName.length() > 30 || birthDay.length() > 2
                    || birthMonth.length() > 2 || birthYear.length() > 4 || nationalCode.length() != 10) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);

            } else {
                Customer customer = new Customer();
                try {
                    int birthdayD = Integer.parseInt(birthDay);
                    int birthdayM = Integer.parseInt(birthMonth);
                    int birthdayY = Integer.parseInt(birthYear);
                    if (birthdayD < 1 || birthdayD > 31 || birthdayM < 1 || birthdayM > 12 || birthdayY < 1000 || birthdayY > 1500)
                        throw new Exception("Invalid Date.");
                    int[] birthdayConvert = Converter.jalali_to_gregorian(birthdayY, birthdayM, birthdayD);
                    Date birthday = Date.valueOf(birthdayConvert[0] + "-" + birthdayConvert[1] + "-" + birthdayConvert[2]);
                    customer.setFirstName(firstName);
                    customer.setLastName(lastName);
                    customer.setFatherName(fatherName);
                    customer.setBirthday(birthday);
                    Long.parseLong(nationalCode);
                    customer.setNationalCode(nationalCode);
                    customer.setCustomerType(customerType.getTypeIndex());

                } catch (Exception e) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }

                CustomerDao customerDao = new CustomerDao();
                Customer tempCustomer = new Customer();
                tempCustomer.setNationalCode(customer.getNationalCode());
                tempCustomer.setCustomerType(customer.getCustomerType());
                List<Customer> customerList = customerDao.select(tempCustomer);

                if (!(customerList.isEmpty())) {
                    customer.setId(customerList.get(0).getId());
                    request.setAttribute("real", customer);
                    request.setAttribute("found", true);
                    request.getRequestDispatcher("/customer/real/add-real-result.jsp").forward(request, response);
                    return;
                }
                request.setAttribute("found", false);
                customerDao.insert(customer);
                CustomerDao cDao = new CustomerDao();
                customerList = cDao.select(customer);
                customer.setId(customerList.get(0).getId());
                request.setAttribute("customer", customer);
                request.getRequestDispatcher("/customer/real/add-real-result.jsp").forward(request, response);

            }

        } else if (customerType.equals(CustomerType.LEGAL)) {

            if (firstName == null || firstName.trim().equals("") || birthDay == null || birthDay.trim().equals("")
                    || birthMonth == null || birthMonth.trim().equals("") || birthYear == null ||
                    birthYear.trim().equals("") || nationalCode == null || nationalCode.trim().equals("")) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);

            } else if (birthDay.length() > 2 || birthMonth.length() > 2 || birthYear.length() > 4 ||
                    (!(nationalCode.length() == 12 || nationalCode.length() == 16) || firstName.length() > 45)) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);

            } else {
                Customer customer = new Customer();
                try {
                    int birthdayD = Integer.parseInt(birthDay);
                    int birthdayM = Integer.parseInt(birthMonth);
                    int birthdayY = Integer.parseInt(birthYear);
                    if (birthdayD < 1 || birthdayD > 31 || birthdayM < 1 || birthdayM > 12 || birthdayY < 1000 || birthdayY > 1500)
                        throw new Exception("Invalid Date.");
                    int[] birthdayConvert = Converter.jalali_to_gregorian(birthdayY, birthdayM, birthdayD);
                    Date birthday = Date.valueOf(birthdayConvert[0] + "-" + birthdayConvert[1] + "-" + birthdayConvert[2]);
                    customer.setBirthday(birthday);
                    Long.valueOf(nationalCode);
                    customer.setNationalCode(nationalCode);
                    customer.setFirstName(firstName);
                    customer.setCustomerType(customerType.getTypeIndex());

                } catch (Exception e) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                CustomerDao customerDao = new CustomerDao();
                Customer tempCustomer = new Customer();
                tempCustomer.setNationalCode(customer.getNationalCode());
                tempCustomer.setCustomerType(customer.getCustomerType());
                List<Customer> customerList = customerDao.select(tempCustomer);

                if (!(customerList.isEmpty())) {
                    customer.setId(customerList.get(0).getId());
                    request.setAttribute("customer", customer);
                    request.setAttribute("found", true);
                    request.getRequestDispatcher("/customer/legal/add-legal-result.jsp").forward(request, response);
                    return;

                }
                request.setAttribute("found", false);
                customerDao.insert(customer);
                CustomerDao cDao = new CustomerDao();
                customerList = cDao.select(customer);
                customer.setId(customerList.get(0).getId());
                request.setAttribute("customer", customer);
                request.getRequestDispatcher("/customer/legal/add-legal-result.jsp").forward(request, response);

            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }
}
