package com.dotin.customermanager.dao;

import com.dotin.customermanager.model.Customer;
import com.dotin.customermanager.service.DBConnector;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


public class CustomerDao {

    private static final Logger logger = Logger.getLogger("global");

    private static final String INSERT_CUSTOMER = "INSERT INTO `user`.`customer` (`national_code`, `first_name`, `last_name`, `father_name`, `birthday`, `customer_type`)" +
            " VALUES (?,?,?,?,?,?)";

    private static final String UPDATE_CUSTOMER = "UPDATE `user`.`customer` " +
            "SET `national_code` = ?, `first_name` = ?, `last_name` = ?, `father_name` = ?, `birthday` = ? WHERE (`id` = ?)";

    private static final String DELETE_PERSON = "DELETE FROM `user`.`customer` WHERE (`id` = ?)";

    private String SELECT_CUSTOMER = "SELECT * FROM user.customer ";


    public void insert(Customer customer) {

        DBConnector dbConnector = new DBConnector();
        try {
            PreparedStatement preparedStatement = dbConnector.getConnection().prepareStatement(INSERT_CUSTOMER);
            preparedStatement.setString(1, customer.getNationalCode());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getFatherName());
            preparedStatement.setDate(5, customer.getBirthday());
            preparedStatement.setInt(6, customer.getCustomerType());
            preparedStatement.execute();
            logger.info("Inserted. NationalCode=" + customer.getNationalCode());
        } catch (SQLException e) {
            logger.info("Insert Error." + e.getMessage());
        } finally {
            dbConnector.closeConnection();
        }


    }

    public int update(Customer customer) {
        DBConnector dbConnector = new DBConnector();
        int affected = 0;
        try {
            PreparedStatement preparedStatement = dbConnector.getConnection().prepareStatement(UPDATE_CUSTOMER);
            preparedStatement.setString(1, customer.getNationalCode());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getFatherName());
            preparedStatement.setDate(5, customer.getBirthday());
            preparedStatement.setInt(6, customer.getId());
            affected = preparedStatement.executeUpdate();
            logger.info("updated. NationalCode=" + customer.getNationalCode());
        } catch (SQLException e) {
            logger.info("Update Error." + e.getMessage());
        } finally {
            dbConnector.closeConnection();
        }
        return affected;

    }

    public int delete(Integer customerId) {
        DBConnector dbConnector = new DBConnector();
        int affected = 0;
        try {
            PreparedStatement preparedStatement = dbConnector.getConnection().prepareStatement(DELETE_PERSON);
            preparedStatement.setInt(1, customerId);
            affected = preparedStatement.executeUpdate();
            logger.info("deleted. CustomerId=" + customerId);

        } catch (SQLException e) {
            logger.info("Delete Error." + e.getMessage());
        } finally {
            dbConnector.closeConnection();
        }

        return affected;
    }

    public List<Customer> select(Customer customer) {
        int findIndex = 1;
        int typeIndex = 0;
        int idIndex = 0;
        int firstNameIndex = 0;
        int lastNameIndex = 0;
        int nationalCodeIndex = 0;
        List<Customer> customerList = new ArrayList<Customer>();

        if (customer.getCustomerType() != null) {
            SELECT_CUSTOMER += " where customer_type= ?";
            typeIndex = findIndex;
            findIndex++;
        } else {
            return customerList;
        }

        if (customer.getId() != null) {
            SELECT_CUSTOMER += " and id=?";
            idIndex = findIndex;
            findIndex++;
        }
        if (customer.getFirstName() != null) {
            SELECT_CUSTOMER += " and first_name=?";
            firstNameIndex = findIndex;
            findIndex++;
        }
        if (customer.getLastName() != null) {
            SELECT_CUSTOMER += " and last_name=?";
            lastNameIndex = findIndex;
            findIndex++;
        }
        if (customer.getNationalCode() != null) {
            SELECT_CUSTOMER += " and national_code=?";
            nationalCodeIndex = findIndex;
            findIndex++;
        }

        DBConnector dbConnector = new DBConnector();
        try {
            PreparedStatement preparedStatement = dbConnector.getConnection().prepareStatement(SELECT_CUSTOMER);
            if (typeIndex > 0)
                preparedStatement.setInt(typeIndex, customer.getCustomerType());
            if (idIndex > 0)
                preparedStatement.setInt(idIndex, customer.getId());
            if (firstNameIndex > 0)
                preparedStatement.setString(firstNameIndex, customer.getFirstName());
            if (lastNameIndex > 0)
                preparedStatement.setString(lastNameIndex, customer.getLastName());
            if (nationalCodeIndex > 0)
                preparedStatement.setString(nationalCodeIndex, customer.getNationalCode());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Customer tempCustomer = new Customer();
                tempCustomer.setCustomerType(resultSet.getInt("customer_type"));
                tempCustomer.setFirstName(resultSet.getString("first_name"));
                tempCustomer.setLastName(resultSet.getString("last_name"));
                tempCustomer.setFatherName(resultSet.getString("father_name"));
                tempCustomer.setBirthday(resultSet.getDate("birthday"));
                tempCustomer.setId(resultSet.getInt("id"));
                tempCustomer.setNationalCode(resultSet.getString("national_code"));
                customerList.add(tempCustomer);

            }
            resultSet.close();
            preparedStatement.close();

        } catch (SQLException e) {
            logger.info("Select Error." + e.getMessage());
        } finally {
            dbConnector.closeConnection();
        }
        return customerList;

    }

}
