package com.dotin.customermanager.enums;

public enum CustomerType {
    REAL(0), LEGAL(1);

    final int typeIndex;

    CustomerType(int typeIndex) {
        this.typeIndex = typeIndex;
    }

    public int getTypeIndex() {
        return typeIndex;
    }

    public static CustomerType getCustomerType(int index) {
        for (CustomerType customerType : CustomerType.values()) {
            if (customerType.getTypeIndex() == index)
                return customerType;
        }
        return null;
    }

}
